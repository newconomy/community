<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        $root = Role::create(['name' => config('access.users.root_role')]);
        $admin = Role::create(['name' => config('access.users.admin_role')]);
        $user = Role::create(['name' => config('access.users.default_role')]);

        // Create Permissions
        $permissions = [
            'view root',
            'view backend'
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        // ALWAYS GIVE ROOT ROLE ALL PERMISSIONS
        $root->givePermissionTo(Permission::all());

        // Assign Permissions to other Roles
        $admin->givePermissionTo('view backend');

        $this->enableForeignKeys();
    }
}
